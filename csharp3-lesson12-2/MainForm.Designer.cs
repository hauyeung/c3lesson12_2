﻿namespace csharp3_lesson12_2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iterationsLabel = new System.Windows.Forms.Label();
            this.originalLabel = new System.Windows.Forms.Label();
            this.stringLabel = new System.Windows.Forms.Label();
            this.builderLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.iterationsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.originalTextBox = new System.Windows.Forms.TextBox();
            this.convertButton = new System.Windows.Forms.Button();
            this.stringIggyLabel = new System.Windows.Forms.Label();
            this.builderIggyLabel = new System.Windows.Forms.Label();
            this.expressionIggyLabel = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.translationcomboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iterationsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // iterationsLabel
            // 
            this.iterationsLabel.AutoSize = true;
            this.iterationsLabel.Location = new System.Drawing.Point(22, 26);
            this.iterationsLabel.Name = "iterationsLabel";
            this.iterationsLabel.Size = new System.Drawing.Size(50, 13);
            this.iterationsLabel.TabIndex = 0;
            this.iterationsLabel.Text = "Iterations";
            // 
            // originalLabel
            // 
            this.originalLabel.AutoSize = true;
            this.originalLabel.Location = new System.Drawing.Point(22, 113);
            this.originalLabel.Name = "originalLabel";
            this.originalLabel.Size = new System.Drawing.Size(42, 13);
            this.originalLabel.TabIndex = 1;
            this.originalLabel.Text = "Original";
            // 
            // stringLabel
            // 
            this.stringLabel.AutoSize = true;
            this.stringLabel.Location = new System.Drawing.Point(25, 166);
            this.stringLabel.Name = "stringLabel";
            this.stringLabel.Size = new System.Drawing.Size(34, 13);
            this.stringLabel.TabIndex = 2;
            this.stringLabel.Text = "String";
            // 
            // builderLabel
            // 
            this.builderLabel.AutoSize = true;
            this.builderLabel.Location = new System.Drawing.Point(22, 221);
            this.builderLabel.Name = "builderLabel";
            this.builderLabel.Size = new System.Drawing.Size(69, 13);
            this.builderLabel.TabIndex = 3;
            this.builderLabel.Text = "String Builder";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Regular Expression";
            // 
            // iterationsNumericUpDown
            // 
            this.iterationsNumericUpDown.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.iterationsNumericUpDown.Location = new System.Drawing.Point(101, 26);
            this.iterationsNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.iterationsNumericUpDown.Name = "iterationsNumericUpDown";
            this.iterationsNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.iterationsNumericUpDown.TabIndex = 5;
            this.iterationsNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // originalTextBox
            // 
            this.originalTextBox.Location = new System.Drawing.Point(101, 113);
            this.originalTextBox.Name = "originalTextBox";
            this.originalTextBox.Size = new System.Drawing.Size(401, 20);
            this.originalTextBox.TabIndex = 6;
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(539, 109);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 23);
            this.convertButton.TabIndex = 7;
            this.convertButton.Text = "Convert";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // stringIggyLabel
            // 
            this.stringIggyLabel.AutoSize = true;
            this.stringIggyLabel.Location = new System.Drawing.Point(137, 166);
            this.stringIggyLabel.Name = "stringIggyLabel";
            this.stringIggyLabel.Size = new System.Drawing.Size(16, 13);
            this.stringIggyLabel.TabIndex = 8;
            this.stringIggyLabel.Text = "...";
            // 
            // builderIggyLabel
            // 
            this.builderIggyLabel.AutoSize = true;
            this.builderIggyLabel.Location = new System.Drawing.Point(137, 221);
            this.builderIggyLabel.Name = "builderIggyLabel";
            this.builderIggyLabel.Size = new System.Drawing.Size(16, 13);
            this.builderIggyLabel.TabIndex = 9;
            this.builderIggyLabel.Text = "...";
            // 
            // expressionIggyLabel
            // 
            this.expressionIggyLabel.AutoSize = true;
            this.expressionIggyLabel.Location = new System.Drawing.Point(137, 274);
            this.expressionIggyLabel.Name = "expressionIggyLabel";
            this.expressionIggyLabel.Size = new System.Drawing.Size(16, 13);
            this.expressionIggyLabel.TabIndex = 10;
            this.expressionIggyLabel.Text = "...";
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(538, 326);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 11;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // translationcomboBox
            // 
            this.translationcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.translationcomboBox.FormattingEnabled = true;
            this.translationcomboBox.Items.AddRange(new object[] {
            "t to tt",
            "s to ss"});
            this.translationcomboBox.Location = new System.Drawing.Point(101, 63);
            this.translationcomboBox.Name = "translationcomboBox";
            this.translationcomboBox.Size = new System.Drawing.Size(121, 21);
            this.translationcomboBox.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Translations";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 379);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.translationcomboBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.expressionIggyLabel);
            this.Controls.Add(this.builderIggyLabel);
            this.Controls.Add(this.stringIggyLabel);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.originalTextBox);
            this.Controls.Add(this.iterationsNumericUpDown);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.builderLabel);
            this.Controls.Add(this.stringLabel);
            this.Controls.Add(this.originalLabel);
            this.Controls.Add(this.iterationsLabel);
            this.Name = "MainForm";
            this.Text = "IggyLatin";
            ((System.ComponentModel.ISupportInitialize)(this.iterationsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label iterationsLabel;
        private System.Windows.Forms.Label originalLabel;
        private System.Windows.Forms.Label stringLabel;
        private System.Windows.Forms.Label builderLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown iterationsNumericUpDown;
        private System.Windows.Forms.TextBox originalTextBox;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Label stringIggyLabel;
        private System.Windows.Forms.Label builderIggyLabel;
        private System.Windows.Forms.Label expressionIggyLabel;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.ComboBox translationcomboBox;
        private System.Windows.Forms.Label label1;
    }
}

