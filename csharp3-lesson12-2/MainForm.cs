﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;


namespace csharp3_lesson12_2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(originalTextBox.Text))
            {
                int loops = (int)iterationsNumericUpDown.Value;
                Stopwatch stopWatch = new Stopwatch();
                string result = "";
                long elapsedTicks = 0;

                // Run through stop watch the first time (per Microsoft) to ensure more accurate measurements
                stopWatch.Start();
                stopWatch.Stop();
                stopWatch.Reset();

                // String
                stopWatch.Start();
                for (int i = 0; i < loops; i++)
                {
                    if (translationcomboBox.Text == "t to tt")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "t", "tt"));
                    }
                    else if (translationcomboBox.Text == "s to ss")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "s", "ss"));
                    }
                }
                stopWatch.Stop();
                elapsedTicks = stopWatch.ElapsedTicks;
                stringIggyLabel.Text = string.Format("{0} - {1}", elapsedTicks, result);
                // StringBuilder
                stopWatch.Reset();
                stopWatch.Start();
                for (int i = 0; i < loops; i++)
                {
                    if (translationcomboBox.Text == "t to tt")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "t", "tt"));
                    }
                    else if (translationcomboBox.Text == "s to ss")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "s", "ss"));
                    }
                }
                stopWatch.Stop();
                elapsedTicks = stopWatch.ElapsedTicks;
                builderIggyLabel.Text = string.Format("{0} - {1}", elapsedTicks, result);
                // Regular Expression
                stopWatch.Reset();
                stopWatch.Start();
                for (int i = 0; i < loops; i++)
                {
                    if (translationcomboBox.Text == "t to tt")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "t", "tt"));
                    }
                    else if (translationcomboBox.Text == "s to ss")
                    {
                        result = IggyConversion.ConvertUsingString(Regex.Replace(originalTextBox.Text, "s", "ss"));
                    }
                }
                stopWatch.Stop();
                elapsedTicks = stopWatch.ElapsedTicks;
                expressionIggyLabel.Text = string.Format("{0} - {1}", elapsedTicks, result);


            }

        }

       

    }
}
